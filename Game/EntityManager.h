#pragma once
#include "Entity.h"
#include "Player.h"
#include "Ball.h"
#include "GameEngine.h"

class EntityManager {
private:
	Ball* ball;
	Player* player;
	std::vector<Entity*> entities;

public:
	EntityManager(TextureCache* cache);

	void update(GameEngine* game);
	void render(GameEngine* game);

	~EntityManager();

	Ball* getBall() { return ball; }
	Player* getPlayer() { return player; }
	std::vector<Entity*>* getEntities() { return &entities; }
};

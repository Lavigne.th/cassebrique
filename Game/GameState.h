#pragma once
#include "GameEngine.h"

class GameState
{
public:
	virtual void Init() = 0;
	virtual void Cleanup() = 0;

	virtual void Pause() = 0;
	virtual void Resume() = 0;

	virtual void handleEvents(GameEngine* game) = 0;
	virtual void update(GameEngine* game) = 0;
	virtual void render(GameEngine* game) = 0;

	void ChangeState(GameEngine* game,
		GameState* state) {
		game->changeState(state);
	}

protected: GameState() { }
};

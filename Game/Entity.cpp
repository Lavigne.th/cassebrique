#include "Entity.h"

Entity::Entity(const Vector2& position) {
	this->position = position;
}

Entity::Entity(
	const Vector2 position,
	TextureCache* cache,
	const std::string& texturePath, 
	const int width,
	const int height
	) : width(width), height(height){
	setPosition(position);
	sprite.setTexture(
		*cache->getTexture(texturePath)
	);

	processSpriteDimension();
	this->keyboardManager = EventHandler::getInstance();
}

Entity::Entity(
	const Vector2 position,
	TextureCache* cache,
	const std::string& texturePath
) {

	sprite.setTexture(
		*cache->getTexture(texturePath)
	);
	this->width = (int)sprite.getLocalBounds().width;
	this->height = (int)sprite.getLocalBounds().height;
}


void Entity::processSpriteDimension() {
	sprite.setScale(sf::Vector2f(
		1.f / (sprite.getLocalBounds().width / (float)width),
		1.f / (sprite.getLocalBounds().height / (float)height)
	));
}

void Entity::setWidth(const int width) {
	this->width = width;
	processSpriteDimension();
}
void Entity::setHeight(const int height) {
	this->height = height;
	processSpriteDimension();
}

int Entity::getWith() const {
	return this->width;
}

int Entity::getHeight() const {
	return this->height;
}

void Entity::render(GameEngine* game) {
	float x = position.x + game->getScreenWidth() / 2.0f - width / 2;
	float y = -position.y + game->getScreenHeight() - height / 2;
	sprite.setPosition(x, y);
	game->getWindow()->draw(this->sprite);
}

void Entity::setPosition(const Vector2& v) {
	this->position.x = v.x ;
	this->position.y = v.y ;
}

Vector2 Entity::getPosition() {
	return this->position;
}


void Entity::move(float xMove, float yMove) {
	setPosition(Vector2(position.x + xMove, position.y + yMove));
}

float Entity::getSpeed() {
	return this->speed;
}

void Entity::setSpeed(float speed) {
	this->speed = speed;
}

bool Entity::intersectBall(const Entity& ball) {
	float cx = ball.position.x;
	float cy = ball.position.y;
	float testX = cx;
	float testY = cy;
	if (cx < position.x)         testX = position.x;        // left edge
	else if (cx > position.x + width) testX = position.x + width;     // right edge

	if (cy < position.y)         testY = position.y;        // top edge
	else if (cy > position.y + height) testY = position.y + height;     // bottom edge

	float distX = cx - testX;
	float distY = cy - testY;
	float distance = sqrt((distX*distX) + (distY*distY));

	return distance < ball.getWith() / 2.0f;
}
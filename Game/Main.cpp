#include "GameEngine.h"

void main() {
	GameEngine* game = GameEngine::getInstance();

	game->init(1024, 800);

	while (game->isRunning())
	{
		game->handleEvents();
		game->update();
		game->render();
	}
	game->cleanup();
}
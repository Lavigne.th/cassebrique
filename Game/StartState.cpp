#include "StartState.h"
#include "PlayState.h"

StartState StartState::INSTANCE;

void StartState::Init() {
	menu = new GUIGroup(GameEngine::getInstance()->getGui());

	int middleScreen = (int)(GameEngine::getInstance()->getScreenHeight() / 2.0f - 53);

	menu->setBackground("res/btn.png", GUIGroup::getCenterPosition(600, 500), Vector2(600, 500));

	tgui::BitmapButton::Ptr play = tgui::BitmapButton::create();
	play->setPosition("(parent.size - size) / 2", middleScreen - 107 - 50 );
	play->setImage("res/play.png");
	play->setSize(496, 107);
	play->connect("pressed", startStatePlayBtn);
	menu->add(play, "PLAY");

	tgui::BitmapButton::Ptr button2 = tgui::BitmapButton::create();
	button2->setPosition("(parent.size - size) / 2", middleScreen);
	button2->setImage("res/load.png");
	button2->setSize(496, 107);
	button2->connect("pressed", startStateLoadBtn);
	menu->add(button2, "LOAD");

	tgui::BitmapButton::Ptr quitBtn = tgui::BitmapButton::create();
	quitBtn->setPosition("(parent.size - size) / 2", middleScreen + 107 + 50);
	quitBtn->setImage("res/quit.png");
	quitBtn->setSize(496, 107);
	quitBtn->connect("pressed", startStateQuitBtn);
	menu->add(quitBtn, "QUIT");

	
}

void startStateQuitBtn() {
	GameEngine::getInstance()->quit();
}

void startStatePlayBtn() {
	GameEngine::getInstance()->changeState(PlayState::getInstance());
}

void startStateLoadBtn() {
	PlayState::getInstance()->setLoadGame(true);
	GameEngine::getInstance()->changeState(PlayState::getInstance());
}

void StartState::Cleanup() {
	menu->clear();
	delete menu;
}

void StartState::Pause() {

}

void StartState::Resume() {

}

void StartState::handleEvents(GameEngine* game) {

}

void StartState::update(GameEngine* game) {
	
}

void StartState::render(GameEngine* game) {

}
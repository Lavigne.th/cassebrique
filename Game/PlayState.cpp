#include "PlayState.h"
#include "StartState.h"

PlayState PlayState::INSTANCE;

void PlayState::Init() {
	entityManager = new EntityManager(GameEngine::getInstance()->getTextureCache());
	if (loadGame)
		load();

	createMenu();
	createPopUp();
}
void PlayState::createMenu() {
	menu = new GUIGroup(GameEngine::getInstance()->getGui());

	int middleScreen = (int)(GameEngine::getInstance()->getScreenHeight() / 2.0f - 53);
	menu->setBackground("res/btn.png", GUIGroup::getCenterPosition(600, 500), Vector2(600, 500));

	tgui::BitmapButton::Ptr resumeBtn = tgui::BitmapButton::create();
	resumeBtn->setPosition("(parent.size - size) / 2", middleScreen - 107 - 50);
	resumeBtn->setImage("res/resume.png");
	resumeBtn->setSize(496, 107);
	resumeBtn->connect("pressed", playStateResume);
	menu->add(resumeBtn, "RESUME");

	tgui::BitmapButton::Ptr saveBtn = tgui::BitmapButton::create();
	saveBtn->setPosition("(parent.size - size) / 2", middleScreen);
	saveBtn->setImage("res/save.png");
	saveBtn->setSize(496, 107);
	saveBtn->connect("pressed", playStateSavePopUp);
	menu->add(saveBtn, "SAVE");

	tgui::BitmapButton::Ptr toMainMenuBtn = tgui::BitmapButton::create();
	toMainMenuBtn->setPosition("(parent.size - size) / 2", middleScreen + 107 + 50);
	toMainMenuBtn->setImage("res/mainMenu.png");
	toMainMenuBtn->setSize(496, 107);
	toMainMenuBtn->connect("pressed", playStateToMainMenu);
	menu->add(toMainMenuBtn, "TO_MAIN_MENU");

	menu->hide();
	menuOpen = false;
}

void PlayState::createPopUp() {
	savePopUp = new GUIGroup(GameEngine::getInstance()->getGui());

	int middleScreen = (int)(GameEngine::getInstance()->getScreenHeight() / 2.0f - 53);
	Vector2 bgPos = GUIGroup::getCenterPosition(600, 250);
	savePopUp->setBackground("res/btn.png", bgPos, Vector2(600, 250));

	tgui::Label::Ptr text = tgui::Label::create();
	text->setText("La sauvegarde pr�c�dente sera �cras�e.\nConfirmer?");
	text->setTextSize(25);
	text->setSize(500, 150);
	text->getRenderer()->setTextColor(sf::Color::White);
	text->setPosition(bgPos.x + 50, bgPos.y + 25);
	
	savePopUp->add(text, "CONFIRM_TEXT");

	tgui::BitmapButton::Ptr declineBtn = tgui::BitmapButton::create();
	declineBtn->setPosition(bgPos.x + 150, bgPos.y + 150);
	declineBtn->setImage("res/check.png");
	declineBtn->setSize(75, 75);
	declineBtn->connect("pressed", playStateSave);
	declineBtn->setInheritedOpacity(0);
	declineBtn->setImageScaling(1);
	savePopUp->add(declineBtn, "DECLINE");

	tgui::BitmapButton::Ptr confirmBtn = tgui::BitmapButton::create();
	confirmBtn->setPosition(bgPos.x + 600 - 225, bgPos.y + 150);
	confirmBtn->setImage("res/close.png");
	confirmBtn->setSize(75, 75);
	confirmBtn->setImageScaling(1);
	confirmBtn->setInheritedOpacity(0);
	confirmBtn->connect("pressed", playStateCloseSavePopUp);
	savePopUp->add(confirmBtn, "CONFIRM");

	savePopUp->hide();
	savePopUpOpen = false;
}

void playStateResume() {
	PlayState::getInstance()->closeMenu();
}

void playStateSavePopUp() {
	PlayState::getInstance()->showSavePopUp();
}

void playStateSave() {
	std::ofstream myfile;
	EntityManager* manager = PlayState::getInstance()->getEntityManager();
	Player* player = manager->getPlayer();
	myfile.open("res/save.txt");
	myfile << player->getPosition().x << "\n";
	myfile << player->getPosition().y << "\n";
	myfile << player->getWith() << "\n";
	myfile << player->getHeight() << "\n";
	myfile << player->getSpeed() << "\n\n";

	Ball* ball = manager->getBall();
	myfile << ball->getPosition().x << "\n";
	myfile << ball->getPosition().y << "\n";
	myfile << ball->getWith() << "\n";
	myfile << ball->getHeight() << "\n";
	myfile << ball->getAngle() << "\n";
	myfile << player->getSpeed() << "\n\n";
	
	myfile.close();

	GameEngine::getInstance()->changeState(StartState::getInstance());
}

void PlayState::load() {
	loadGame = false;
	std::ifstream infile("res/save.txt");
	loadPlayer(infile);
	loadBall(infile);
	infile.close();
}

void PlayState::loadPlayer(std::ifstream& file) {
	Player* player = entityManager->getPlayer();
	std::string data[5];
	for (int i = 0; i < 5; i++) 
		std::getline(file, data[i]);

	player->setPosition(Vector2(
		std::stof(data[0]),
		std::stof(data[1])
	));
	player->setWidth(std::stoi(data[2]));
	player->setHeight(std::stoi(data[3]));
	player->setSpeed(std::stof(data[4]));
}

void PlayState::loadBall(std::ifstream& file) {
	Ball* ball = entityManager->getBall();
	std::string data[6];
	std::getline(file, data[0]);
	for (int i = 0; i < 6; i++)
		std::getline(file, data[i]);

	ball->setPosition(Vector2(
		std::stof(data[0]),
		std::stof(data[1])
	));
	ball->setWidth(std::stoi(data[2]));
	ball->setHeight(std::stoi(data[3]));
	ball->setAngle(std::stof(data[4]));
	ball->setSpeed(std::stof(data[5]));
}


void playStateCloseSavePopUp() {
	PlayState::getInstance()->closeSavePopUp();
}

void playStateToMainMenu() {
	GameEngine::getInstance()->changeState(StartState::getInstance());
}

void PlayState::showSavePopUp() {
	savePopUp->show();
	savePopUpOpen = true;
}

void PlayState::closeSavePopUp() {
	savePopUp->hide();
	savePopUpOpen = false;
}

void PlayState::Cleanup() {
	menu->clear();
	delete menu;
	savePopUp->clear();
	delete savePopUp;
	delete entityManager;
}

void PlayState::Pause() {

}

void PlayState::Resume() {

}

void PlayState::handleEvents(GameEngine* game) {
	lastMenuAction += game->getElapsedTime().asSeconds();
	if(lastMenuAction >= 0.5f && !savePopUpOpen)
		if (game->getEventHandler()->getKey(sf::Keyboard::Escape))
			handleMenu();
}

void PlayState::handleMenu() {
	if (!menuOpen) {
		menuOpen = true;
		menu->show();
		lastMenuAction = 0;
	}
	else
		closeMenu();
}

void PlayState::closeMenu() {
	menuOpen = false;
	menu->hide();
	lastMenuAction = 0;
}

void PlayState::update(GameEngine* game) {
	if(!menuOpen)
		entityManager->update(game);
}

void PlayState::render(GameEngine* game) {
	entityManager->render(game);
}
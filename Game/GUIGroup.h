
#pragma once
#include <iostream>
#include <TGUI/TGUI.hpp>
#include "Vector2.h"

class GUIGroup {
private:
	std::vector<std::string> widgetsName;
	tgui::Gui* gui;

	static int MAX_ID;
	std::string id;

	std::string getWidgetName(const std::string& name);
public:
	GUIGroup(tgui::Gui* gui);

	void show();
	void hide();
	void enable();
	void disable();
	void clear();

	void add(const std::shared_ptr<tgui::Widget>& widget, const std::string& name);
	void remove(const std::string& name);

	void setBackground(const std::string& path, const Vector2& position, const Vector2& dimension);

	std::shared_ptr<tgui::Widget> getWidget(const std::string& name);

	static Vector2 getCenterPosition(const int width, const int height);
};
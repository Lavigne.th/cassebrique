#include "EventHandler.h"
#include "GameEngine.h"

EventHandler* EventHandler::INSTANCE = nullptr;
bool EventHandler::keys[256];

EventHandler* EventHandler::getInstance() {
	if (INSTANCE == nullptr) {
		INSTANCE = new EventHandler;
		for (bool b : keys)
			b = false;
	}
	return INSTANCE;
}

void EventHandler::update(GameEngine* game) {
	sf::Event event;
	while (game->getWindow()->pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
			game->getWindow()->close();
		else if (event.type == sf::Event::KeyPressed)
			this->keys[event.key.code] = true;
		else if (event.type == sf::Event::KeyReleased)
			this->keys[event.key.code] = false;

		game->getGui()->handleEvent(event);
	}
}

bool EventHandler::getKey(sf::Keyboard::Key keyCode) {
	return this->keys[keyCode];
}
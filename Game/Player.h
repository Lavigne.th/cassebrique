#pragma once
#include "Entity.h"

class Player : public Entity {
private:

public:
	Player(const Vector2 position, TextureCache* cache, const std::string& path, int width, int height);

	void update(GameEngine* game) override;
};

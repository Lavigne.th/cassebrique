#pragma once
#include <SFML/Graphics.hpp>
#include <iostream>

class GameEngine;

class EventHandler {
private:
	static bool keys[256];
	static EventHandler* INSTANCE;

	EventHandler() {}
public:
	
	void update(GameEngine* game);
	bool getKey(sf::Keyboard::Key keyCode);
	static EventHandler* getInstance();
};

#pragma once

#include "GameState.h"
#include "GUIGroup.h"

class StartState : public GameState {
private:
	GUIGroup* menu;
	static StartState INSTANCE;

	StartState() {}
public:
	void Init();
	void Cleanup();

	void Pause();
	void Resume();

	void handleEvents(GameEngine* game);
	void update(GameEngine* game);
	void render(GameEngine* game);

	static StartState* getInstance() {
		return &INSTANCE;
	}
};

void startStateQuitBtn();
void startStatePlayBtn();
void startStateLoadBtn();
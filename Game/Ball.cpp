#include "Ball.h"

Ball::Ball(const Vector2 position, TextureCache* cache, const std::string& path, int width, int height)
	: Entity(position, cache, path, width, height) {

}

void Ball::update(GameEngine* game, Player& player) {
	sf::Time elapseTime = game->getElapsedTime();
	float moveX = speed * cosine * elapseTime.asSeconds();
	float moveY = speed * sine * elapseTime.asSeconds();
	if (intersectEntity(player, position.x + moveX, position.y + moveY) && position.y > player.getPosition().y + player.getHeight() / 2.0f)
		playerHit(game, elapseTime, player);
	else
		moveBall(game, moveX, moveY);
}

void Ball::playerHit(GameEngine* game, sf::Time& elapsedTime, Player& player) {
	sine = -sine;
	float moveX = speed * cosine * elapsedTime.asSeconds();
	float moveY = speed * sine * elapsedTime.asSeconds();
	
	moveBall(game, moveX, moveY);
}

bool Ball::intersectEntity(Entity& entity, float xPos, float yPos) {
	float cx = xPos;
	float cy = yPos;
	float testX = cx;
	float testY = cy;
	if (cx < entity.getPosition().x - entity.getWith() /2.0f)         testX = entity.getPosition().x - entity.getWith() / 2.0f;        // left edge
	else if (cx > entity.getPosition().x + entity.getWith() / 2.0f) testX = entity.getPosition().x + entity.getWith() / 2.0f;     // right edge

	if (cy < entity.getPosition().y - entity.getHeight() /2.0f)         testY = entity.getPosition().y - entity.getHeight() / 2.0f;        // top edge
	else if (cy > entity.getPosition().y + entity.getHeight() /2.0f) testY = entity.getPosition().y + entity.getHeight() /2.0f;     // bottom edge

	float distX = cx - testX;
	float distY = cy - testY;
	float distance = sqrt((distX*distX) + (distY*distY));
	
	return distance < width / 2.0f;
}

void Ball::update(GameEngine* game) {

}

void Ball::moveBall(GameEngine* game, float moveX, float moveY) {
	float newX = position.x + moveX;
	float newY = position.y + moveY;
	if (
		newY >= game->getScreenHeight() - height / 2.0f
		)
		sine = -sine;
	else if (
		newX >= game->getScreenWidth() / 2.0f - width / 2.0f ||
		newX <= -game->getScreenWidth() / 2.0f + width / 2.0f
		)
		cosine = -cosine;
	else if (newY <= height / 2.0f)
		std::cout << "Game Over" << std::endl;
	else
		move(moveX, moveY);
}

void Ball::setAngle(float angle) {
	float angleRad = angle *  M_PI / 180.0f;
	this->cosine = cosf(angleRad);
	this->sine = sinf(angleRad);
}


void Ball::resetPosition() {
	setPosition(Vector2(0, 200));
}


float Ball::getAngle() {
	double angleRadian = (sine > 0) ? acos(cosine) : -acos(cosine);
	double angleDegrees;
	return angleRadian > 0 ? angleRadian * 180 / M_PI : 360 + angleRadian * 180 / M_PI;
}
#include "Player.h"

Player::Player(const Vector2 position, TextureCache* cache, const std::string& path, int width, int height)
	: Entity(position, cache, path, width, height) {

}

void Player::update(GameEngine* game) {
	float movement = 0;
	sf::Time elapseTime = game->getElapsedTime();
	if (keyboardManager->getKey(sf::Keyboard::E)) {
		movement = -speed * elapseTime.asSeconds();
		float leftLimit = -game->getScreenWidth() / 2.0f + width / 2.0f;
		if (movement != 0 && this->position.x - movement > leftLimit)
			move(movement, 0);
	}
	else if (keyboardManager->getKey(sf::Keyboard::R)) {
		movement = speed * elapseTime.asSeconds();
		float rightLimit = game->getScreenWidth() / 2.0f - width / 2.0f;
		if (movement != 0 && this->position.x + movement < rightLimit)
			move(movement, 0);
	}
}

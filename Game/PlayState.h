#pragma once
#include <fstream>

#include "GameState.h"
#include "GUIGroup.h"
#include "EntityManager.h"

class PlayState : public GameState{
private:
	GUIGroup* menu, *savePopUp;
	static PlayState INSTANCE;

	EntityManager* entityManager;
	bool menuOpen, savePopUpOpen, loadGame;

	PlayState() { }
	void handleMenu();

	float lastMenuAction;
	
	void createMenu();
	void createPopUp();
	void load();
	void loadPlayer(std::ifstream& file);
	void loadBall(std::ifstream& file);

public:
	void Init();
	void Cleanup();

	void Pause();
	void Resume();

	void handleEvents(GameEngine* game);
	void update(GameEngine* game);
	void render(GameEngine* game);

	static PlayState* getInstance() {
		return &INSTANCE;
	}

	void closeMenu();
	GUIGroup* getSavePopUp() { return savePopUp; }

	void showSavePopUp();
	void closeSavePopUp();

	void setLoadGame(bool load) { loadGame = load; }

	EntityManager* getEntityManager() { return entityManager; }
};

void playStateResume();
void playStateSavePopUp();
void playStateSave();
void playStateToMainMenu();
void playStateCloseSavePopUp();
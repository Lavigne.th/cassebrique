#pragma once
#include <iostream>
#include "Vector2.h"
#include "TextureCache.h"
#include "GameEngine.h"

class Entity {
protected:
	Vector2 position;
	sf::Sprite sprite;
	EventHandler* keyboardManager;
	
	int width, height;
	float speed;

	void processSpriteDimension();

public:
	Entity(const Vector2& position);
	Entity(const Vector2 position, TextureCache* cache, const std::string& texturePathconst, int width, const int height);
	Entity(const Vector2 position, TextureCache* cache, const std::string& texturePathconst);

	virtual void update(GameEngine* game) = 0;
	void render(GameEngine* game);

	void setWidth(const int width);
	void setHeight(const int height);
	int getWith() const;
	int getHeight() const;

	void setPosition(const Vector2& v);
	Vector2 getPosition();

	void move(float x, float y);

	float getSpeed();
	void setSpeed(float speed);

	bool intersectBall(const Entity& ball);
};

#include "TextureCache.h"

TextureCache::TextureCache() {

}

sf::Texture* TextureCache::getTexture(const std::string& path) {
	std::map<std::string, sf::Texture*>::iterator texture_id = textures.find(path);

	if (texture_id == textures.end()) {
		sf::Texture* texture = new sf::Texture();
		texture->loadFromFile(path);
		textures.emplace(path, texture);
		return texture;
	}
	return texture_id->second;
}

TextureCache::~TextureCache() {
	for (auto& t : textures) {
		delete t.second;
	}
}

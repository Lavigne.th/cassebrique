#pragma once
#include <SFML/Graphics.hpp>

class TextureCache {
private:
	std::map<std::string, sf::Texture*> textures;

public:
	TextureCache();

	sf::Texture* getTexture(const std::string& name);
	~TextureCache();
};

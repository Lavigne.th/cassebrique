#pragma once
#include <vector>
#include <SFML/Graphics.hpp>
#include <TGUI/TGUI.hpp>

#include "EventHandler.h"
#include "TextureCache.h"

class GameState;
class StartState;

class GameEngine
{
private:

	static GameEngine INSTANCE;
	int screenWIdth, screenHeight;
	
	sf::RenderWindow* window;

	std::vector<GameState*> states;
	bool running;
	EventHandler* eventHandler;

	sf::Clock clock;
	sf::Time elapsedTime;

	tgui::Gui* gui;

	TextureCache textureCache;

	GameEngine() {}

public:
	void init(const int width, const int height);
	void cleanup();

	void changeState(GameState* state);
	void pushState(GameState* state);
	void popState();

	void handleEvents();
	void update();
	void render();

	bool isRunning() { return running; }
	void quit() { running = false; }

	tgui::Gui* getGui();

	static GameEngine* getInstance() { return &INSTANCE; }

	int getScreenWidth() { return screenWIdth; }
	int getScreenHeight() { return screenHeight; }
	TextureCache* getTextureCache() { return &textureCache; }
	EventHandler* getEventHandler() { return eventHandler; }
	sf::Time& getElapsedTime() { return elapsedTime; }
	sf::RenderWindow* getWindow() { return window; }
};

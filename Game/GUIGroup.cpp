#include "GUIGroup.h"
#include "GameEngine.h"

int GUIGroup::MAX_ID = 0;


GUIGroup::GUIGroup(tgui::Gui* gui)
	: gui(gui)
{
	id = std::to_string(MAX_ID);
	MAX_ID++;
}

void GUIGroup::show() {
	for(std::string name : widgetsName)
		gui->get(name)->setVisible(true);

	tgui::Widget::Ptr bg = gui->get(getWidgetName("BG"));
	if (bg != NULL)
		bg->setVisible(true);
}

void GUIGroup::hide() {
	for (std::string name : widgetsName) 
		gui->get(name)->setVisible(false);

	tgui::Widget::Ptr bg = gui->get(getWidgetName("BG"));
	if (bg != NULL)
		bg->setVisible(false);
}
void GUIGroup::enable() {
	for (std::string name : widgetsName)
		gui->get(name)->setEnabled(true);
}
void GUIGroup::disable() {
	for (std::string name : widgetsName)
		gui->get(name)->setEnabled(false);
}

void GUIGroup::clear() {
	for (std::string name : widgetsName)
		gui->remove(gui->get(name));
	gui->remove(gui->get(getWidgetName("BG")));
}

void GUIGroup::remove(const std::string& name) {
	gui->remove(gui->get(getWidgetName(name)));
	std::vector<std::string>::iterator position = std::find(widgetsName.begin(), widgetsName.end(), getWidgetName(name));
	if (position != widgetsName.end())
		widgetsName.erase(position);
	else
		std::cout << "Widget not found" << std::endl;
}

void GUIGroup::add(const std::shared_ptr<tgui::Widget>& widget, const std::string& name) {
	std::string fullName = getWidgetName(name);
	gui->add(widget, fullName);
	widgetsName.push_back(fullName);
}


std::shared_ptr<tgui::Widget> GUIGroup::getWidget(const std::string& name) {
	return gui->get(getWidgetName(name));
}

std::string GUIGroup::getWidgetName(const std::string& name) {
	std::string res;
	return res.append(id).append(name);
}

void GUIGroup::setBackground(const std::string& path, const Vector2& position, const Vector2& dimension) {
	tgui::BitmapButton::Ptr bg = gui->get<tgui::BitmapButton>(getWidgetName("BG"));
	if (bg == NULL)
		bg = tgui::BitmapButton::create();

	bg->setPosition(position.x, position.y);
	bg->setImage("res/btn.png");
	bg->setSize(dimension.x, dimension.y);
	bg->setEnabled(false);
	bg->setImageScaling(10);
	gui->add(bg, getWidgetName("BG"));
}

Vector2 GUIGroup::getCenterPosition(const int width, const int height) {
	GameEngine* game = GameEngine::getInstance();
	float xPos = game->getScreenWidth() / 2.0f - width / 2.0f;
	float yPos = game->getScreenHeight() / 2.0f - height / 2.0f;
	return Vector2(xPos, yPos);
}
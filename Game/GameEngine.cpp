#include "GameEngine.h"
#include "GameState.h"
#include "StartState.h"
#include "PlayState.h"

GameEngine GameEngine::INSTANCE;

void GameEngine::init(const int width, const int height) {
	this->window = new sf::RenderWindow(sf::VideoMode(width, height), "SFML works!");
	this->screenWIdth = width;
	this->screenHeight = height;
	window->setFramerateLimit(60);
	
	eventHandler = EventHandler::getInstance();

	gui = new tgui::Gui(*window);

	changeState(StartState::getInstance());

	running = true;
}

void GameEngine::changeState(GameState* state) {
	if (states.size() != 0) {
		states.back()->Cleanup();
		states.pop_back();
	}
	states.push_back(state);
	states.back()->Init();
}
void GameEngine::pushState(GameState* state) {

}

void GameEngine::popState() {

}

void GameEngine::handleEvents() {
	eventHandler->update(this);
	states.back()->handleEvents(this);
}

void GameEngine::update() {
	
	if (!window->isOpen())
		return quit();

	elapsedTime = clock.restart();
	states.back()->update(this);
}

void GameEngine::render() {
	window->clear(sf::Color::Black);
	
	states.back()->render(this);
	this->gui->draw();

	window->display();
}

void GameEngine::cleanup() {
	window->close();
	delete window;
	delete eventHandler;
	delete gui;
}


tgui::Gui* GameEngine::getGui() {
	return this->gui;
}

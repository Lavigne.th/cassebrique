#include "EntityManager.h"

EntityManager::EntityManager(TextureCache* cache) {
	Vector2	v(0, 0);
	ball = new Ball(v, cache, std::string("res/ball.png"), 50, 50);
	ball->resetPosition();
	ball->setSpeed(400);
	ball->setAngle(135);

	v.y = 50;
	player =new Player(v, cache, std::string("res/rect.png"), 300, 50);
	player->setSpeed(400);
}

void EntityManager::update(GameEngine* game) {
	ball->update(game, *player);
	player->update(game);
	
	for (Entity* e : entities)
		e->update(game);
}

void EntityManager::render(GameEngine* game) {
	ball->render(game);
	player->render(game);

	for (Entity* e : entities)
		e->render(game);
}

EntityManager::~EntityManager() {
	delete player;
	delete ball;
	for (Entity* e : entities)
		delete e;
}
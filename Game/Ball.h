#pragma once
#define _USE_MATH_DEFINES
#include <math.h>
#include "Entity.h"
#include "Player.h"

class Ball : public Entity {
protected:
	float cosine, sine;
	
public :
	Ball(const Vector2 position, TextureCache* cache, const std::string& path, int width, int height);
	
	void update(GameEngine* game, Player& player);
	void update(GameEngine* game)override;

	void setAngle(float angle);
	void moveBall(GameEngine* game, float moveX, float moveY);
	void resetPosition();

	bool intersectEntity(Entity& entity, float xPos, float yPos);
	void playerHit(GameEngine* game, sf::Time& elapsedTime, Player& player);

	float getAngle();

	void gameOver();
};
